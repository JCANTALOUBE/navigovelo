import json

def velyceo():
    """Cette fonction extrait et retourne les coordonnées (latitude, longitude) à partir des données JSON du fichier '244400644_velyceo.json'. Les coordonnées proviennent de la clé 'geom_point' de chaque objet dans le JSON."""
    f = open("244400644_velyceo.json", "rt", encoding='UTF8')
    lecteurJSON = json.loads(f.read())
    c = []
    for objet in lecteurJSON:
        lat, long = objet["geom_point"]["lat"], objet["geom_point"]["lon"]
        c.append([lat, long])
    f.close()
    return c


def accueil_velos():
    """Cette fonction extrait et retourne les coordonnées (latitude, longitude) à partir des données JSON du fichier '793866443_accueil-velo-en-loire-atlantique@loireatlantique.json'. Les coordonnées proviennent des clés 'latitude' et 'longitude' de chaque objet dans le JSON."""
    f = open("793866443_accueil-velo-en-loire-atlantique@loireatlantique.json", "rt", encoding='UTF8')
    lecteurJSON = json.loads(f.read())
    coord = []
    for objet in lecteurJSON:
        lat, long = objet["latitude"], objet["longitude"]
        coord.append([lat, long])
    f.close()
    return coord


def stationnement():
    """Cette fonction extrait et retourne les coordonnées (latitude, longitude) à partir des données JSON du fichier '244400644_stationnements_cyclables.json'. Les coordonnées proviennent de la clé 'coordonneesxy' de chaque objet dans le JSON, converties en valeurs numériques."""
    f = open("244400644_stationnements_cyclables.json", "rt", encoding='UTF8')
    lecteurJSON = json.loads(f.read())
    coordonnees = []
    for objet in lecteurJSON:
        x = objet["coordonneesxy"]
        x = x[1:-1]
        coord = x.split(",")
        lat = float(coord[1])
        lon = float(coord[0])
        coordonnees.append([lat, lon])
    f.close()
    return coordonnees
