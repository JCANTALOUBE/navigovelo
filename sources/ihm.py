import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from PIL import Image, ImageTk
import folium
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import webbrowser
from recupe_donnees import*

class CarteVeloApp:
    def __init__(self, root):
        self.root = root
        # création d'un dictionnaire qui associe les fonctions de recupe_donnees aux options
        self.coordonnees = {
            "Velyceo en libre service": velyceo(),
            "Stationnement": stationnement(),
            "Accueil Velo": accueil_velos(),
        }

        self.option_select = tk.StringVar()
        self.nom_rue = tk.StringVar()
        root.iconbitmap("cycliste.ico")
        self.logo_image = Image.open("logo1.jpg")
        self.logo_photo = ImageTk.PhotoImage(self.logo_image)

        self.setup()


    def setup(self):
        '''Cette fonction mets en place l'IHM'''

        self.root.title("NavigoVélo")
        self.root.geometry('400x300')
        self.root.resizable(False, False)
        image = Image.open("ville.jpg")
        photo = ImageTk.PhotoImage(image)

        # Mise en place de l'image du fond
        label_image = tk.Label(self.root, image=photo)
        label_image.image = photo
        label_image.place(x=0, y=0, relwidth=1, relheight=1)

        # Création d'un rectangle blanc sur l'image
        self.canvas = tk.Canvas(self.root, bg="white", highlightthickness=0)
        self.canvas.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)

        # Création d'un label avec le logo
        label_logo = tk.Label(self.root, image=self.logo_photo)
        label_logo.place(relx=0.5, rely=0.1, anchor="n")

        # Création du menu déroulant
        menu = ttk.Combobox(self.canvas, values=["Veuillez sélectionner", "Stationnement", "Vélycéo en libre service", "Etablissement Accueil Velo"],
                    textvariable=self.option_select, state="readonly", font=("Helvetica", 12), style="TCombobox")
        menu.place(relx=0.5, rely=0.36, anchor="center")
        menu.set("Veuillez sélectionner")

        # Création d'un label qui donne des indications sur la zone de saisie texte
        label_rue = ttk.Label(self.canvas, text="Nom de la rue:", style="TLabel")
        label_rue.place(relx=0.15, rely=0.54, anchor="center")

        # Création de la zone de saisi avec l'exemple de saisie pour l'utilisateur
        entree_rue = ttk.Entry(self.canvas, textvariable=self.nom_rue, font=("Helvetica", 12), style="Example.TEntry")
        entree_rue.place(relx=0.6, rely=0.54, anchor="center")
        entree_rue.insert(0, "exemple : rue Jean Macé")
        entree_rue.bind("<FocusIn>", lambda event: self.clear_entry(event, entree_rue))

        #Création du bouton qui permettra d'afficher la carte
        bouton_carte = ttk.Button(self.canvas, text="Afficher la carte", command=self.show_carte, style='TButton')
        bouton_carte.place(relx=0.5, rely=0.8, anchor="center")


    def clear_entry(self, event, widget_entree):
        ''' Cette fonction efface l'exemple de saisie de la zone de texte
                lorsque l'utilisateur clique dessus'''

        if widget_entree.get() == "exemple : rue Jean Macé":
            widget_entree.delete(0, tk.END)
            widget_entree.configure(style="TEntry")

    def show_carte(self):
        ''' Cette fonction enregistre la carte générée dans le même
            dossier que le programme et ouvre la carte dans une
            page web sur le navigateur par défaut lorsque
            l'utilisateur clique sur le bouton afficher carte'''

        # Vérifie qu'une option du menu déroulant a été séléctionnée
        if self.option_select.get() == "Veuillez sélectionner":
            messagebox.showwarning("Sélection requise", "Veuillez sélectionner une option dans le menu déroulant.")
            return

        # érifie que l'utilisateur a écrit quelque chose dans la zone de saisi
        if not self.nom_rue.get():
            messagebox.showwarning("Saisie requise", "Veuillez saisir le nom de la rue.")
            return

        adresse_complete = f"{self.nom_rue.get()}, Saint-Nazaire, France"
        geolocator = Nominatim(user_agent="carte_velo_app")

        # Si les coordonnées de la rue sont introuvables un message d'erreur s'affiche
        try:
            location = geolocator.geocode(adresse_complete, timeout=10)
            if location is None:
                messagebox.showerror("Coordonnées introuvables", f"Les coordonnées pour {self.nom_rue.get()} n'ont pas été trouvées.")
                return
        # Si les géocodage prends du temps alors un message d'erreur s'affiche
        except GeocoderTimedOut:
            messagebox.showerror("Erreur de géocodage", "Le géocodage a pris trop de temps. Veuillez réessayer.")
            return

        map = folium.Map(location=[location.latitude, location.longitude], zoom_start=30)

        # Boucle pour afficher des markers pour chaque endroits
        coordonnees_infrastructure = self.coordonnees.get(self.option_select.get(), [])
        for coord in coordonnees_infrastructure:
            folium.Marker([coord[0], coord[1]], popup=self.option_select.get()).add_to(map)

        # Enregistrement de la carte et affichage d'un message pour informer l'utilisateur
        map.save("carte_velo.html")
        webbrowser.open('carte_velo.html')


if __name__ == "__main__":
    root = tk.Tk()
    app = CarteVeloApp(root)
    root.mainloop()